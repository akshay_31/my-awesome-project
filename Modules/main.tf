# Creating 1st VPC
resource "aws_vpc" "VPC1" {
  cidr_block       = var.cidr_block1
  instance_tenancy = "default"

  tags = {
    Name = "1st-VPC"
  }
}

# Creating 2nd VPC
#resource "aws_vpc" "VPC-2" {
 # cidr_block       = var.cidr_block2
  #instance_tenancy = "default"
#
 # tags = {
  #  Name = "2nd-VPC"
  #}
#}

# Creating Internet Gateway
# Create Internet Gateway and Attach it to VPC
# terraform aws create internet gateway
resource "aws_internet_gateway" "IG" {
  vpc_id    = aws_vpc.VPC1.id

  tags      = {
    Name    = "Test IGW"
  }
}

#Creating 1st Public Subnet
resource "aws_subnet" "pub-sub1" {
  vpc_id     = aws_vpc.VPC1.id
  cidr_block = var.pub_sub
  availability_zone        = "us-east-1a"
  map_public_ip_on_launch  = true

  tags = {
    Name = "Public-Subnet1"
  }
}

#Creating Route Tables
#Creating RT for Public Subnet
resource "aws_route_table" "pub-RT" {
  vpc_id = aws_vpc.VPC1.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.IG.id
    }

  tags = {
    Name = "public-RT"
  }
}

#Creating RT for Private Subnet
resource "aws_default_route_table" "default-RT" {
    default_route_table_id = aws_vpc.VPC1.default_route_table_id
    tags = {
          Name = "default-route"
    }
}


# Associate Public Subnet 1 to "Public Route Table"
# terraform aws associate subnet with route table
resource "aws_route_table_association" "pub-sub1-RT-association" {
  subnet_id           = aws_subnet.pub-sub1.id
  route_table_id      = aws_route_table.pub-RT.id
}

# Associate Private Subnet to "Private Route Table"
resource "aws_route_table_association" "pri-sub1-RT-association" {
  subnet_id = aws_subnet.pri-sub1.id
  route_table_id = aws_default_route_table.default-RT.id
}


resource "aws_nat_gateway" "NAT-GW" {
  allocation_id = aws_eip.EIP.id
  subnet_id     = aws_subnet.pub-sub1.id

  tags = {
    Name = "NAT-GW"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  #depends_on = [aws_internet_gateway.IG.id]
}

#Creating Elastic IP
resource "aws_eip" "EIP" {
  vpc      = true
}

#Creating EC-2 Instance
resource "aws_instance" "app" {
  ami = var.ec2-ami
  instance_type = var.ec2-IT

user_data = <<EOF
				#!/bin/bash
				sudo yum update -y && sudo yum install -y docker
				sudo systemctl start docker
				sudo usermod -aG docker ec2-user
				docker run -p 8080:80 nginx
				EOF
  tags = {
    Name = "VM-1"
  }
}

#Creating 1st Private Subnet
resource "aws_subnet" "pri-sub1" {
  vpc_id     = aws_vpc.VPC1.id
  cidr_block = var.pri_sub
  availability_zone        = "us-east-1b"
  map_public_ip_on_launch  = false

  tags = {
    Name = "Private-Subnet1"
  }
}
