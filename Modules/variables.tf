variable "cidr_block1" {
  description = "1st CIDR Block"
}

variable "instance_tenancy" {
  default = "default"
}

#variable "cidr_block2" {
 # description = "2nd CIDR Block"
#}

variable "pub_sub" {
  default = []
  description = "1st Public Subnet"
}

variable "pri_sub" {
  default = []
  description = "1st Private Subnet"
}

#variable "AWS_REGION" {
 #   type = string
  #  default = "us-east-1"
#}

variable "ec2-IT" {
  description = "Define Instance Type"
}

variable "ec2-ami" {
  description = "Define ami used"
}

