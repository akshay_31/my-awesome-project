
variable "AWS_REGION" {
  description = "AWS Region"
}


variable "vpc_cidr" {
  description = "The CIDR block for the VPC."
}

variable "pub-subnet-app" {
  default = []

}

variable "pri-subnet-app" {
 default = []
}

variable "ec2-AMI" {
 description = "AMI"
}

variable "ec2-it" {
 description = "Instance Type"
}

